// popup.js
// lib.js
function f1(){
  chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
    chrome.scripting.executeScript(
        {
          target: {tabId: tabs[0].id},
          function: function() {
                var css = document.createElement("style");
                var head = document.head;
                head.appendChild(css);

                css.type = 'text/css';

                css.innerText = `* {
                    -webkit-user-select: text !important;
                    -moz-user-select: text !important;
                    -ms-user-select: text !important;
                     user-select: text !important;
                }`;
              [].forEach.call(['contextmenu', 'copy', 'cut', 'paste', 'mouseup', 'mousedown', 'keyup', 'keydown', 'drag', 'dragstart', 'select', 'selectstart'], function(event) {
                document.addEventListener(event, function(e) {
                e.stopPropagation();
                }, true);
              });
          }
    });
  });
}

function loadSettings(mdl) {
  if (localStorage['mdl1']) {
    var text = localStorage['mdl1'];
    for (var i = 0; i < mdl.options.length; i++) {
      if (mdl.options[i].text === text) {
          mdl.selectedIndex = i;
          break;
      }
  }
  }
}

function saveSettings(mdl) {
  localStorage['mdl1'] = mdl.value;
}

function make_login_data(){
  
  var display_name = document.getElementById("display_name").value;
  var email_login = document.getElementById("email_login").value;
  var pass = document.getElementById("pass").value;
  
  var login_data = {
    "type": "login_data",
    "display_name": display_name,
    "email_login": email_login,
    "pass": pass
  }
  console.log(login_data);
  console.log(JSON.stringify(login_data));
  return JSON.stringify(login_data);
}

function enc_dta_data(data){
  return btoa(data);
}

function dec_dta_data(data){
  return atob(data);
}

function download(filename, text) {
  var element = document.createElement('a');
  element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));
  element.setAttribute('download', filename);

  element.style.display = 'none';
  document.body.appendChild(element);

  element.click();

  document.body.removeChild(element);
}

function read_login_file(){
  document.addEventListener('DOMContentLoaded', (event) => {
    const dropZone = document.body;
  
    dropZone.addEventListener('dragover', (event) => {
      event.stopPropagation();
      event.preventDefault();
      event.dataTransfer.dropEffect = 'copy';
    });
  
    dropZone.addEventListener('drop', (event) => {
      event.stopPropagation();
      event.preventDefault();
      const files = event.dataTransfer.files;
      if (files.length > 0) {
        const file = files[0];
        const reader = new FileReader();
        reader.onload = (event) => {
          var data = dec_dta_data(event.target.result)
          var login_data = JSON.parse(data);
          var type = login_data["type"]
          console.log(type);
          if (type != 'login_data'){
            console.error("err_file_format");
            return;
          }
          else{
            var display_name = login_data["display_name"];
            var email_login = login_data["email_login"];
            var pass = login_data["pass"];
            auto_login(email_login,pass);

          }
          
        };
        reader.readAsText(file);
      }
    });
  });
}

function auto_login(login, pass) {
  chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
    chrome.scripting.executeScript(
        {
          target: {tabId: tabs[0].id},
          function: function(login, pass) {
            document.getElementById("login").value = login;
            document.getElementById("pass").value = pass;
          },
          args: [login, pass]
    });
  });
}
// lib.js

var qw = "0";
// logs
let logs_txt = document.getElementById("logs_txt");
document.getElementById("logs").setAttribute("hidden", true);
logs_vis = true;

document.getElementById('logs_btn').addEventListener('click', () => {
  
  if (logs_vis) {
    document.getElementById("logs").removeAttribute("hidden");
    logs_vis = false;
  }
  else {
    document.getElementById("logs").setAttribute("hidden", true);
    logs_vis = true;
  }
});

// setings
document.getElementById("settings").setAttribute("hidden", true);
setings_vis = true;
document.getElementById('settings_btn').addEventListener('click', () => {
  
  if (setings_vis) {
    document.getElementById("settings").removeAttribute("hidden");
    setings_vis = false;
  }
  else {
    document.getElementById("settings").setAttribute("hidden", true);
    setings_vis = true;
  }
});
// // setings_ auto re try

btn = document.getElementById('pswd');
btn.addEventListener('click', () => {

  key = document.getElementById('key');
  btn.textContent = 's';
  key.type = Text;
});

// auto_login
read_login_file()

// model
var mld1 = document.getElementById("mdl1");
mld1.addEventListener("change", () => {
  saveSettings(mld1);
  mld1.ind
});
loadSettings(mld1)


// sync_logs 
setInterval(()=> {
  let url = "http://127.0.0.1:9494/logs/read/data"
  fetch(url).then(response =>{ response.text().then(text => {
      logs_txt.value = text;
    })
  });
}, 400);

// get_server_status
function ping_server(){
  fetch('http://127.0.0.1:9494/get-status', {method: 'GET'})
  .then(response => response.text().then(text => {
    if(text === "server_online"){
      document.getElementById("st_d").style = "background-color: #19ad19";
    }
    else if(text === "server_timeout") {
      document.getElementById("st_d").style = "background-color: #FF8400";
    }
    else if(text === "server_err") {
      document.getElementById("st_d").style = "background-color: #454545";
    }
  }))
  .catch(err => {document.getElementById("st_d").style = "background-color: red";});
}
ping_server();
setInterval(()=> {
  ping_server();
}, 5000);


// use utils

if (localStorage.getItem('f1')){
  f1();
}

// page utils
document.getElementById("utils").addEventListener('click', function() {
  chrome.tabs.create({
    url: 'utils_page.html'
  });
  window.close();
});

// page info
document.getElementById("info_btn").addEventListener('click', function() {
  chrome.tabs.create({
    url: 'info.html'
  });
  window.close();
});
// kk
function qqq(inputValue){
  if(inputValue==="136664" || inputValue==="0136664" ){ // 7355608
    f1();
    if (localStorage.getItem('f1')){
      localStorage.removeItem("f1");
      return true;
    }
    localStorage.setItem("f1", 1);
  return true;
  }
}

window.onload = function() {
  let key = localStorage.getItem('key');
  if (key) {
    document.getElementById('key').value = key;
  }
  let logs = localStorage.getItem("s_logs");
  console.log(logs);
  if (logs){
    logs_txt.value = logs;
  }
  
};

document.getElementById('result').value = qw;

document.getElementById('testButton').addEventListener('click', () => {
  
  let inputValue = document.getElementById('result').value;
  let key = document.getElementById('key').value;

  localStorage.setItem('key', key);
  if (qqq(inputValue)){

  }
  else{
  qwe(inputValue, key, mld1.value);
  }
});
// jаvаsсrірt-оbfusсаtоr:dіsаblе
function qwe(inputValue, key, mdl){

  chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
    chrome.scripting.executeScript(
        {
            target: {tabId: tabs[0].id},
            function: async function(inputValue, key, mdl) {


              


              console.log(key);
              console.log(mdl);
              function preventSelection(element) {
                console.log(1)
              }
              preventSelection(document);
              function isimg() {
                let divsrow = document.querySelectorAll('.group_form')[0].querySelectorAll('.row');
                // .setAttribute("style", "background-color: #00ff00") // debug  
                //divsrow[q["id"]].setAttribute("style", "background-color: #00ff00"); 
                let question = divsrow[parseInt(inputValue)];
                
                let img = question.querySelector('img');
                
                //console.log(img.width);
                //var base64 = getBase64Image(img);
                
                //alert(base64);
                return img;
              }

              let img_str = "";

              try {
              let img = isimg();
              let srs = img.src;
              // console.log(1);
              let prt = img.parentElement;

              // prt.setAttribute("style", "background-color: #00ff00");

              let node = document.createTextNode("This is new.");
              
              
              var canvas = document.createElement('canvas');
              
              prt.appendChild(canvas);
              var context = canvas.getContext('2d');
              // console.log(2);

              // Set the canvas dimensions to the image dimensions
              canvas.height = img.naturalHeight;
              canvas.width = img.naturalWidth;

              // console.log(3);

              // Draw the image onto the canvas
              context.drawImage(img, 0, 0);
              img.crossOrigin = "anonymous";
              //  console.log(4);
              const c = prt.querySelector("Canvas");
              // Get the image data as a Base64 string
              var base64String = c.toDataURL();
              canvas.remove();
              // console.log(5);
              var pureBase64String = base64String.substring(base64String.indexOf(',') + 1);
              img_str = pureBase64String;
              //alert(pureBase64String);
              }
              
              catch (e) {
              console.error(e);
                
              
              }
              
              
              preventSelection(document);
              function sendHTMLContentToServer(htmlContent, inputValue, key) {
                function logs(s) {
                  let url = "http://127.0.0.1:9494/logs/add/"+ s;
                  // var xmlHttp = new XMLHttpRequest();
                  // xmlHttp.open( "GET", url, false ); // false for synchronous request
                  // xmlHttp.send( null );
                  // return xmlHttp.responseText;
                  fetch(url);
                }
                let url = 'http://127.0.0.1:9494/get-help';
                
                fetch(url, {
                  method: 'POST', 
                  headers: {
                      'Content-Type': 'application/json'
                  },
                  body: JSON.stringify({
                    "key" : key,
                    "id" : inputValue,
                    "data": htmlContent, 
                    "img" : img_str,
                    "md" : mdl
                  })
                })
                .then(response =>{ response.text().then(text => {
                  
                  
                  try {
                    let q = JSON.parse(JSON.parse(text));
                  }
                  catch{
                    logs("q " + inputValue + " status Err");
                  }
                  
                  let q = JSON.parse(JSON.parse(text));
                  let divsrow = document.querySelectorAll('.group_form')[0].querySelectorAll('.row');
                  // console.log(divsrow[q["id"]].querySelector("ul"));
                  // .setAttribute("style", "background-color: #00ff00") // debug  
                  //divsrow[q["id"]].setAttribute("style", "background-color: #00ff00"); 
                  let question = divsrow[q["id"]].querySelector("ul");
                  
                  console.log("leng");
                  console.log(q["res"].length);
                  if (q["res"] === "nodata"){
                    logs("q " + inputValue + " status Err");
                    return;
                  }
                  logs("q " + inputValue + " status Ok");
                  for(var i = 0; i < q["res"].length; i++)
                  {
                    
                    let check = question.children[q["res"][i]];
                    check.setAttribute("style", "border-radius:20px");
                    check.querySelector("input").setAttribute("checked", "checked");
                    
                    
                  }

                  
                })
                  
                })
                .then(data => console.log(data))
                .catch((error) => {
                  console.error('Error:', error);
                  logs("q " + inputValue + " status Service Err");
                });
                
              }
            
              var updatedHTMLContent = document.documentElement.outerHTML;
              sendHTMLContentToServer(updatedHTMLContent, inputValue, key);








              let info = inputValue;
             
            },
            args: [inputValue, key, mdl]
        }
    );
  });
  
}
// jаvаsсrірt-оbfusсаtоr:еnаblе


document.getElementById('clc0').addEventListener('click', () => {

  qw+='0';
  document.getElementById('result').value = qw;
  
});

document.getElementById('clc1').addEventListener('click', () => {

  qw+='1';
  document.getElementById('result').value = qw;
  
});

document.getElementById('clc2').addEventListener('click', () => {

  qw+='2';
  document.getElementById('result').value = qw;
  
});

document.getElementById('clc3').addEventListener('click', () => {

  qw+='3';
  document.getElementById('result').value = qw;
  
});

document.getElementById('clc4').addEventListener('click', () => {

  qw+='4';
  document.getElementById('result').value = qw;
  
});

document.getElementById('clc5').addEventListener('click', () => {

  qw+='5';
  document.getElementById('result').value = qw;
  
});

document.getElementById('clc6').addEventListener('click', () => {

  qw+='6';
  document.getElementById('result').value = qw;
  
});

document.getElementById('clc7').addEventListener('click', () => {

  qw+='7';
  document.getElementById('result').value = qw;
  
});

document.getElementById('clc8').addEventListener('click', () => {

  qw+='8';
  document.getElementById('result').value = qw;
  
});

document.getElementById('clc9').addEventListener('click', () => {

  qw+='9';
  document.getElementById('result').value = qw;
  
});


document.getElementById('dll').addEventListener('click', () => {

  qw=qw.slice(0, -1);
  document.getElementById('result').value = qw;
  
});
